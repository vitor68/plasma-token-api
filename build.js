const { registerTransforms } = require('@tokens-studio/sd-transforms');
const StyleDictionary = require('style-dictionary')
const baseConfig = require('./config.json')

registerTransforms(StyleDictionary, {
    excludeParentKeys: true,
    expand: {
        composition: true,
        typography: true,
        border: true,
        shadow: true
    }
});

StyleDictionary.registerTransform({
    name: 'typeAtEnd',
    type: 'value',
    // // matcher: (prop) => prop.attributes.category === 'color',
    // matcher: (token) => {},
    // // transformer: (prop) => `${prop.value}--color`
    // transformer: (token) => {
    //     return token; // <-- transform as needed
    // }

    matcher: token => ['fontSizes', 'dimension', 'borderRadius', 'spacing'].includes(token.type),
    transformer: function(prop, options) {
        let result = '';
        if (prop.path.length == 2) {
            result = prop.path.join('--');
            console.log("result: ", result);
        } else if (prop.path.length == 3) {
            result = `${prop.path[0]}__${prop.path[1]}--${prop.path[2]}`;
            console.log("result: ", result);
        } else if (prop.path.length == 4) {
            result = `${prop.path[0]}__${prop.path[1]}-${prop.path[2]}--${prop.path[3]}`;
            console.log("result: ", result);
        } 
        return result;
    },
});

const StyleDictionaryExtended = StyleDictionary.extend(baseConfig)
StyleDictionaryExtended.buildAllPlatforms()